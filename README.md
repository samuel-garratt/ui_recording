# UI recording with Zalenium

## Credits

Cross-browser Testing Platform and Open Source ❤️ provided by [Sauce Labs][homepage]

[homepage]: https://saucelabs.com

Dashboard and video recording provided by [Zalenium](https://github.com/zalando/zalenium).

## Summary

This repo demonstrates how to use [Zalenium](https://github.com/zalando/zalenium) to video record UI tests. It shows how Zalenium can be started within a CI job in 3 different CI systems. You can see a presentation walking through lines of code [here](https://gitpitch.com/samuel-garratt/ui_recording?grs=gitlab#/)
* For Gitlab it shows the use of using docker in docker that uses a docker-compose file to network UI tests to a running Zalenium instance. The recordings are archived within a Gitlab job from which one can see videos in a Zalenium dashboard. Example is [here](https://samuel-garratt.gitlab.io/-/ui_recording/-/jobs/415191175/artifacts/tmp/dashboard.html) and in any of the artifacts of the recent [CI pipelines](https://gitlab.com/samuel-garratt/ui_recording/pipelines).  
* For Jenkins it shows how this same docker-compose network can be run on a Jenkins Agent that has docker-compose installed configured within a `Jenkinsfile` in this repo.
* For Github actions it demos how to record a UI test at https://github.com/SamuelGarrattIqa/ui_recording.

Currently these tests run in both Chrome and Firefox. Feel free to fork this repo, copy its 
configuration and raise issues if there are parts that are unclear or don't work.

## Mobile recording

This test will also demonstrate how mobile recording can be achieved and testing in other browsers that are not dockerized can be acheived using [Saucelabs](https://saucelabs.com). 

## Explanation of getting video recording going for different CI systems

### Using github actions

Within the GitHub Action marketplace there is a [Zalenium Server template](https://github.com/marketplace/actions/zalenium-server) 
that can be easily added to an action YAML and then tests simply point to `http://localhost:4444/wd/hub` for the provided 
Selenium Grid. To add this all one needs to add is 

```yaml
- name: Zalenium Server 
  uses: ajinx/zalenium@1.1
```

The videos produced from this can be archived using

```yaml
- name: Upload artifact
  uses: actions/upload-artifact@v1.0.0
  with:
    # Artifact name
    name: Zalenium recording
    # Directory containing files to upload
    path: /tmp/videos
```

A sample project using this is available [here](https://github.com/SamuelGarrattIqa/ui_recording). 
The action run with the video artifact is [here](https://github.com/SamuelGarrattIqa/ui_recording/runs/421937913?check_suite_focus=true) 
One simple point to notice is that a sleep (I gave it 20 seconds) is added after the UI tests have finished to allow video files to have finished generating
before stopping Zalenium.

### Within Jenkins

The same came by done within Jenkins for a static agent. Using docker-compose one can define a network of containers that will
be used to perform a task (in this case running a UI test suite and recording the UI). 

An example project is available at https://gitlab.com/samuel-garratt/ui_recording that has a Jenkinsfile setup to run in this way. 
See this [gitpitch presentation](https://gitpitch.com/samuel-garratt/ui_recording?grs=gitlab#/)
for a more detailed walkthrough of the `.gitlab-ci.yml` and `Jenkinsfile` presented below.  

The docker-compose file below setups a Zalenium container within a network that the test container runs against.

```yaml
version: '3.7'

services:
  #--------------#
  zalenium:
    image: "dosel/zalenium"
    hostname: zalenium
    tty: true
    volumes:
      - ./tmp:/home/seluser/videos
      - /var/run/docker.sock:/var/run/docker.sock
    ports:
      - 4444 # Using just container port (an ephemeral host port is chosen)
    command: start --sendAnonymousUsageInfo false
    environment:
      TZ: Pacific/Auckland
      PULL_SELENIUM_IMAGE: "true"
  test:
    build: .
    command: ./run_tests_when_ready.sh
    volumes:
      - .:/mysuite
    depends_on:
      - zalenium
    environment:
      WEBDRIVER_URL: http://zalenium:4444/wd/hub # the WebDriver container, referenced by its service name
      WEBDRIVER_CHROMEOPTIONS: start-maximized disable-popup-blocking no-sandbox disable-dev-shm-usage # additional chrome args separated by a space
      remote: "true"
      BROWSER: "${BROWSER}"
```

The test container has a script that waits until the service is ready before running the tests.

The `docker-compose` command `docker-compose up --abort-on-container-exit --exit-code-from test` will run the
test container and exit once it has finished.

A `Jenkinsfile` that performs this command for a Chrome and Firefox browser is shown below.

```ruby
pipeline {
    agent {
        label 'docker-compose'
    }
    options { buildDiscarder(logRotator(numToKeepStr: '2')) }
    stages {
        stage('browsers') {
            matrix {
                agent {
                    node {
                        label 'docker-compose'
                        customWorkspace "~/.jenkins/workspace/recording_${BROWSER}"
                    }
                }
                axes {
                    axis {
                        name 'BROWSER'
                        values 'chrome', 'firefox'
                    }
                }
                stages {
                    stage('Test') {
                        steps {
                            sh 'docker-compose up --abort-on-container-exit --exit-code-from test'
                            sh 'docker run -v $PWD/manual:/source jagregory/pandoc --from=markdown --to=docx --output=test.docx training.md'
                        }
                        post {
                            always {
                                junit 'logs/*.xml'
                                publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: true, reportDir: 'tmp', reportFiles: 'dashboard.html',
                                            reportName: "Video recording ${BROWSER}"])
                                archiveArtifacts artifacts: 'manual/test.docx', onlyIfSuccessful: true
                            }
                        }
                    }
                }
            }
        }
    }
}
```

> Note how a pandoc step is run after the docker-compose command. This creates a training doc from the UI tests and will
> be explained in a future blog. 

### Within gitlab ci adding in docker-dind

Since gitlab runs within a docker image itself, an extra service needs to be provided to the `docker-compose` solution so
a docker socket (at `/var/run/docker.sock`) is available to share with the Zalenium container (so it can create it's own containers).

Following is the `.gitlab-ci.yml` that performs this:

```yaml
.job_template: &job_definition
  services:
    - docker:19.03.5-dind
  image: docker:latest
  stage: test
  script:
    - apk add docker-compose
    - docker-compose version
    - docker-compose up --abort-on-container-exit --exit-code-from test
    - docker run -v $PWD/manual:/source jagregory/pandoc --from=markdown --to=docx --output=test.docx training.md
  artifacts:
    paths:
      - manual/
      - tmp/  # Where Zalenium dashboard is stored
      - logs/*.xml
    reports:
      junit: logs/*.xml
    expire_in: 1 week
    when: always

chrome:
  <<: *job_definition
  variables:
    BROWSER: chrome
``` 

Gitlab is awesome enough to automatically create a static website that hosts the Zalenium dashboard and embeds the videos which can be 
seen [here](https://samuel-garratt.gitlab.io/-/ui_recording/-/jobs/415191175/artifacts/tmp/dashboard.html). The artifacts
for this run can be browsed [here](https://gitlab.com/samuel-garratt/ui_recording/-/jobs/415191175/artifacts/browse).

