#!/usr/bin/env bash
# exit if a command returns a non-zero exit code and also print the commands and their args as they are executed
set -e -x
# Wait for Zalenium to be up and running
timeout 300 bash -c 'while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' zalenium:4444/wd/hub/status)" != "200" ]]; do sleep 5; done' || false
bundle install
bundle exec rake setup
bundle exec parallel_cucumber features --group-by scenarios
